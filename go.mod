module gitlab.com/testle/expect

go 1.12

require (
	github.com/ghodss/yaml v1.0.0
	github.com/sergi/go-diff v1.1.0
)
